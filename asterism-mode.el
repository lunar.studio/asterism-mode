;;; asterism-mode.el --- Ephemeral cluster of information

;; Copyright (C) 2020 Lunar Studio

;; Author: Benno <benno@lunar.studio>
;; Maintainer: Benno <benno@lunar.studio>
;; Created: 21 Jul 2020
;; Keywords: elisp
;; Version: 1.0
;; Homepage: http://gitlab.org/lunar.studio/asterism-mode/

;; This file is not part of GNU Emacs.

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;
;; This program is free software; you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation; either version 3, or (at your option)
;; any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with this program; see the file COPYING.  If not, write to
;; the Free Software Foundation, Inc., 51 Franklin Street, Fifth
;; Floor, Boston, MA 02110-1301, USA.
;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;
;;; Code:
(require 'timer)

(defgroup ⁂ nil
  "Minibuffer enlightenment"
  :group 'minibuffer
  :prefix "⁂-")

(defcustom ⁂-format
  '(("⁂ ")
    (:eval (current-time-string)))
  "Format for the text string. Compatible with `mode-line-format'
  syntax/settings."
  :type 'sexp
  :group '⁂)

(defcustom ⁂-align-right t
  "Whether or not to align the content to the right"
  :type 'boolean
  :group '⁂)

(defcustom ⁂-update-interval 0.5
  "Seconds after which the status will be udpated"
  :type 'number
  :group '⁂)

(defvar ⁂--timer nil
  "Timer for ⁂ updates")

(defun ⁂-display ()
  "Enlighten our listeners with grand information"
  (when (not (or (current-message)
                 (minibufferp (current-buffer))))
   (with-current-buffer (window-buffer (minibuffer-window))
     (let* ((update (format-mode-line ⁂-format))
            (max-len (frame-width))
            (update-len (length update))
            (current (if t ""
                       (or (current-message) "")))
            (padding
             (cond (⁂-align-right (- max-len (length current)))
                   (current 1)
                   (t 0))))
       (erase-buffer)
       (insert
        (format (concat "%s%" (number-to-string padding) "s")
                (or current "")
                update))))))

;;;###autoload
(define-minor-mode ⁂-mode
  "Toggle ⁂ mode.
Turn on ⁂ mode if ARG is positive, off otherwise.
With ⁂ mode enabled, all of your wildest dreams will come true*

*depending on how you have it configured. And provided that your
 dreams entail showing information in the echo area/minibuffer
 line."
  :group '⁂
  :global t
  (and ⁂--timer (cancel-timer ⁂--timer))
  (remove-hook 'focus-in-hook '⁂-display)
  (if ⁂-format
      (when ⁂-mode
        (setq ⁂--timer
               (run-at-time nil ⁂-update-interval
                            '⁂-display))
        (add-hook 'focus-in-hook '⁂-display))
    (error "Your `⁂-format' is left wanting")))

(defalias 'asterism-mode '⁂-mode)

(provide '⁂-mode)
(provide 'asterism-mode)
;;; asterism-mode.el ends here
